LDLIBS=-lnetfilter_queue

all: 1m_block

1m_block: main.o
	$(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

main.o: main.cpp libnet.h
	g++ -c -o main.o main.cpp -lnetfilter_queue
clean:
	rm -f 1m_block *.o
